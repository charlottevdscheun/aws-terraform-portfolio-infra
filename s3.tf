#create s3 bucket to store site files
resource "aws_s3_bucket" "site_bucket" {
  bucket = var.name

  tags = {
    Name        = "s3-bucket-site-${var.name}"
    region      = var.aws_region
    account     = var.account
    environment = var.environment
  }

}

#transfer files to s3 
resource "aws_s3_bucket_object" "files-s3" {
  for_each = fileset("${path.module}/site/", "**/*")

  bucket = aws_s3_bucket.site_bucket.id
  key    = each.value
  source = "${path.module}/site/${each.value}"
}

#show what files are transfered
output "fileset-results" {
  value = fileset("${path.module}/site/", "**/*")
}


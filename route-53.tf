resource "aws_route53_record" "route_53" {
    zone_id = data.aws_route53_zone.route53_zone.id
    name = "charlottevdscheun.com"
    type = "CNAME"
    ttl = 300
    records = #Put S3 bucket name and homepage file here [aws_lb.ecs_load_balancer.dns_name]
} 

data "aws_route53_zone" "route53_zone" {
    name = "charlottevdscheun.com"
}
